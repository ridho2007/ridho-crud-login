package com.crud.react.modal;

import javax.persistence.*;

@Entity
@Table(name = "carts")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "produk_id")
    private Produk produkId;

    @Column(name = "qty")
    private  Long qty;
    @Column(name = "total_harga")
    private  float totalharga;
    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users usersId;

    public float getTotalharga() {
        return totalharga;
    }

    public void setTotalharga(float totalharga) {
        this.totalharga = totalharga;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Produk getProdukId() {
        return produkId;
    }

    public void setProdukId(Produk produkId) {
        this.produkId = produkId;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }
}
