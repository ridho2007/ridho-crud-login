package com.crud.react.modal;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Produk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String nama;
    @Lob
    @Column(name = "despripsi")
    private String deskripsi;
    @Column(name = "harga")
    private int harga;

    @Lob
    @Column(name = "img")
    private String img;

    public Produk(String nama, String deskripsi,String img, int harga) {
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
        this.img = img;
    }

    public Produk() {
    }

    @Override
    public String toString() {
        return "Produk{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", harga=" + harga +
                ", img='" + img + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
