package com.crud.react.serviceImplements;


import com.crud.react.exception.InternalErrorException;
import com.crud.react.modal.BarangImage;
import com.crud.react.repository.BarangImageRepository;
import com.crud.react.repository.ProdukRepository;
import com.crud.react.service.BarangImageService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class BarangImageServiceImpl implements BarangImageService {

    private  static final String DOWNLOAD_URL="https://firebasestorage.googleapis.com/v0/b/project-akhir-react.appspot.com/o/%s?alt=media";


    @Autowired
    BarangImageRepository barangImageRepository;

    @Override
    public List<BarangImage> findAll(){
        return barangImageRepository.findAll();
    }

    @Override
    public BarangImage addImage( MultipartFile multipartFile) {
        BarangImage addImage = new BarangImage();
        addImage.setImgeUrl(imageConverter(multipartFile));
        return barangImageRepository.save(addImage);
    }
//1

    private  String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = converTofile(multipartFile,fileName);
            var RESPONSE_URL =uploadFile(file,fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e){
            e.getStackTrace();
            throw new InternalErrorException("eror uplod file");
        }
    }
    private File converTofile(MultipartFile multipartFile,String fileName)throws  IOException{
        File file = new File(fileName);
        try(FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private  String uploadFile(File file,String fileName)throws IOException{
        BlobId blobId = BlobId.of("project-akhir-react.appspot.com",fileName) ;
        BlobInfo blobInfo =BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtenions(String fileName){
        return fileName.split("\\.")[0];
    }
}
