package com.crud.react.serviceImplements;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.repository.ProdukRepository;
import com.crud.react.modal.Produk;
import com.crud.react.service.ProdukService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProdukServiceImpl implements ProdukService {

    @Autowired
    ProdukService produkService;
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/project-akhir-react.appspot.com/o/%s?alt=media";




    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("error upload file");
        }
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("project-akhir-react.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }


    @Autowired
    ProdukRepository produkRepository;


    @Override
    public Page<Produk> getAllproduk(Long page,String search) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 8);
        return produkRepository.searchfindAll(search, pageable);
    }

    @Override
    public Produk addProduk(Produk produk, MultipartFile multipartFile) {
        String img = imageConverter(multipartFile);
        Produk product1 = new Produk(produk.getNama(), produk.getDeskripsi(), img, produk.getHarga());
        return produkRepository.save(product1);
    }


    @Override
    public Produk getProduk(Long id) {
        var produk = produkRepository.findById(id).get();
        return produkRepository.save(produk);
    }

    @Override
    public Produk editProduk(Long id,Produk produk,MultipartFile multipartFile) {
        Produk produk1 = produkRepository.findById(id).get();
        String img = imageConverter(multipartFile);
        produk1.setNama(produk.getNama());
        produk1.setDeskripsi(produk.getDeskripsi());
        produk1.setHarga(produk.getHarga());
        produk1.setImg(img);


        return produkRepository.save(produk1);
    }

    @Override
    public void deleteProdukById(Long id) {
        produkRepository.deleteById(id);
    }

}


