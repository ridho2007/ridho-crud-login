package com.crud.react.serviceImplements;

import com.crud.react.dto.CartDTO;
import com.crud.react.exception.NotFoundException;
import com.crud.react.modal.Cart;
import com.crud.react.modal.Produk;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.ProdukRepository;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {
@Autowired
private UsersRepository usersRepository;

@Autowired
private CartRepository cartRepository;

@Autowired
private ProdukRepository produkRepository;

    @Override
    public Cart create(CartDTO cart) {
    Cart create = new Cart();
Produk produk =produkRepository.findById(cart.getProdukId()).orElseThrow(()-> new NotFoundException("Produk id tidak ditemukan"));
    create.setQty(cart.getQty());
    create.setTotalharga(produk.getHarga()*cart.getQty());
    create.setUsersId(usersRepository.findById(cart.getUsersId()).orElseThrow(()-> new NotFoundException("User Id Tidak Ditemukan")));

    create.setProdukId(produk);

        return cartRepository.save(create);
    }

    @Override
    public Page<Cart> findAll(Long page,Long usersId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page),5);

        return cartRepository.findAll(Long.valueOf(usersId),pageable);
    }

@Override
public  Map<String, Boolean> deleteAll(Long usersId){
    List<Cart>cartList=cartRepository.findAllProductByUserId(usersId);
    cartRepository.deleteAll(cartList);
    Map<String, Boolean> obj = new HashMap<>();
    obj.put("deleted", Boolean.TRUE);
    return obj;
}

    @Override
    public Cart update(Long id, CartDTO cart) {
        Cart data = cartRepository.findById(id).orElseThrow(()->new NotFoundException("id Not Found"));
        data.setQty(cart.getQty());
        data.setUsersId(usersRepository.findById(cart.getUsersId()).orElseThrow(()-> new NotFoundException("User Tidak Ditemukan")));
        data.setProdukId(produkRepository.findById(cart.getProdukId()).orElseThrow(()->new NotFoundException("Produk Id Tidak Ditemukan")));
        return cartRepository.save(data);
    }

    @Override
    public Map<String,Object> delete(Long id) {
        cartRepository.deleteById(id);
        Map<String,Object>obj=new HashMap<>();
        obj.put("DELETED",true);
        return obj;

    }
}
