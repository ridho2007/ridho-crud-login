package com.crud.react.controller;

import com.crud.react.dto.ProdukDto;
import com.crud.react.modal.Produk;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import com.crud.react.service.ProdukService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequestMapping("/Produk")
    @RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ProdukController {

        @Autowired
        private ProdukService produkService;

        @Autowired
        private ModelMapper modelMapper;

    @GetMapping("/{id}")
    public CommonResponse  getProduk(@PathVariable("id") Long id) {
        return ResponHelper.ok(produkService.getProduk(id)) ;
    }
    @GetMapping("/all")
    public CommonResponse<Page<Produk>> getAllproduk(@RequestParam(name = "page")Long page,@RequestParam(required = false) String search){
        return ResponHelper.ok(produkService.getAllproduk(page,search == null ? "": search)) ;
    }
    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Produk> addProduk(ProdukDto produkDto, @RequestPart("file") MultipartFile multipartFile ) {
        return ResponHelper.ok(produkService.addProduk(modelMapper.map(produkDto, Produk.class
        ),multipartFile )) ;
    }
        @PutMapping(path = "/{id}", consumes = "multipart/form-data")
        public CommonResponse<Produk> editProdukById(@PathVariable("id") Long id, ProdukDto produkDto,@RequestPart("file") MultipartFile multipartFile) {
            return ResponHelper.ok( produkService.editProduk(id, modelMapper.map(produkDto, Produk.class), multipartFile));
        }
        @DeleteMapping("/{id}")
        public void deleteProdukById(@PathVariable("id") Long id) { produkService.deleteProdukById(id);}


}

