package com.crud.react.controller;

import com.crud.react.dto.CartDTO;
import com.crud.react.modal.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "http://localhost:3000")
public class CartContoroller {
    @Autowired
    private CartService cartService;
    @PostMapping
    public CommonResponse <Cart> create(@RequestBody CartDTO cart){
        return ResponHelper.ok( cartService.create(cart));

    }
    @GetMapping
    public CommonResponse <Page<Cart>> findAll(@RequestParam Long page,@RequestParam(required = false) Long usersId){
        return ResponHelper.ok (cartService.findAll(page,usersId == null ? Long.valueOf("") :usersId));
    }
    @PutMapping("/id")
    public CommonResponse <Cart> update(@PathVariable("id")Long id, @RequestBody CartDTO cart){
        return ResponHelper.ok (cartService.update(id,cart));
    }
    @DeleteMapping("/{id}")
    public CommonResponse <Map<String,Object>>delete(@PathVariable("id")Long id){
        return ResponHelper.ok (cartService.delete(id));
    }
    @DeleteMapping("/{tuku}")
    public CommonResponse <Map<String,Boolean>>deleteAll(@RequestParam(required = false) Long usersId){
        return ResponHelper.ok (cartService.deleteAll(usersId == null ? Long.valueOf(""):usersId));
    }
}
