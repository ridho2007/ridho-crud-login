package com.crud.react.controller;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.ProfilDTO;
import com.crud.react.dto.UserDto;

import com.crud.react.modal.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import com.crud.react.service.UsersService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RequestMapping("/user")
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UsersController {
    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

@PostMapping("/sign-in")
public CommonResponse<Map<String,Object>>login(@RequestBody LoginDto loginDto){
    return ResponHelper.ok(usersService.login(loginDto));
}
@PostMapping(path = "/sign-up",consumes = "multipart/form-data")
public CommonResponse<Users>addUsers( UserDto userDto, @RequestPart("foto") MultipartFile multipartFile ){
    return ResponHelper.ok(usersService.addUsers(modelMapper.map(userDto,Users.class),multipartFile));
}
    @GetMapping("/all")
    public  Object getAllUsers(){
        return ResponHelper.ok(usersService.getAllUsers());

    }
    @GetMapping("/{id}")
    public CommonResponse<Users> getUsers(@PathVariable("id")Long id){
        return ResponHelper.ok(usersService.getUsers(id));
    }

//        @PostMapping
//    public CommonResponse<Users> addUsers(@RequestBody Users users) {
//        return ResponseHelper.ok( usersService.addUsers(users)) ;
//    }
    @PutMapping(path = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Users> editUsersById(@PathVariable("id") Long id, ProfilDTO profilDTO, @RequestPart("file")MultipartFile multipartFile) {
        return ResponHelper.ok( usersService.editUsers( id,modelMapper.map(profilDTO, Users.class),multipartFile ));
    }
    @DeleteMapping("/{id}")
    public void deleteUsersById(@PathVariable("id") Long id) { usersService.deleteUsersById(id);}
}
