package com.crud.react.controller;


import com.crud.react.modal.BarangImage;
import com.crud.react.service.BarangImageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/image")
public class BarangImageController {

    @Autowired
    BarangImageService barangImageService;
    @PostMapping(consumes = "multipart/form-data")
    public BarangImage addImage( @RequestPart("file")MultipartFile multipartFile) {
        return barangImageService.addImage( multipartFile);
    }

    @GetMapping
    public List<BarangImage> getAll() {
        return  barangImageService.findAll();
    }
}
