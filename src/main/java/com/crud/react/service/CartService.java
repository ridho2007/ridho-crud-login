package com.crud.react.service;

import com.crud.react.dto.CartDTO;
import com.crud.react.modal.Cart;
import com.crud.react.modal.Users;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface CartService {
    Cart create(CartDTO cart);
    Page<Cart>findAll(Long page,Long usersId);

    Cart update(Long id,CartDTO cart);


    Map<String,Boolean> deleteAll(Long usersId);
    Map<String,Object> delete(Long id);
}
