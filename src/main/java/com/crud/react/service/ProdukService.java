package com.crud.react.service;

import com.crud.react.modal.Produk;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProdukService {
    Page<Produk> getAllproduk(Long page,String search);
    Produk addProduk(Produk produk, MultipartFile multipartFile);


    Object getProduk(Long id);
    Produk editProduk(Long id,Produk produk,MultipartFile multipartFile);

    void deleteProdukById(Long id);
}
