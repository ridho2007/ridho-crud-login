package com.crud.react.service;

import com.crud.react.modal.BarangImage;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BarangImageService {
    BarangImage addImage(MultipartFile multipartFile);

    List<BarangImage>findAll();
}
