package com.crud.react.repository;

import com.crud.react.modal.Produk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProdukRepository extends JpaRepository<Produk,Long> {
    @Query(value = "SELECT * FROM products  WHERE nama LIKE CONCAT('%', ?1, '%')",nativeQuery = true)
    Page<Produk> searchfindAll(String search, Pageable pageable);
    }
