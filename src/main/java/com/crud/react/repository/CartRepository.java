package com.crud.react.repository;

import com.crud.react.modal.Cart;

import com.crud.react.modal.Produk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long> {
    @Query(value = "SELECT * FROM carts  WHERE users_id= :usersId",nativeQuery = true)
    Page<Cart> findAll(Long usersId, Pageable pageable);

    @Query(value = "SELECT * FROM carts  WHERE users_id= :usersId",nativeQuery = true)
    List<Cart> findAllProductByUserId(Long usersId);

}
