package com.crud.react.repository;

import com.crud.react.modal.BarangImage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BarangImageRepository extends JpaRepository<BarangImage,Long> {
}
