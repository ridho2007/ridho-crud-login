package com.crud.react.repository;


import com.crud.react.modal.Cart;
import com.crud.react.modal.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface UsersRepository extends JpaRepository <Users,Long> {

    Optional<Users> findByEmail(String email);
    Boolean existsByEmail(String email);

}
